export const LEFT_MENU_ALL_STATE = 'all';
export const LEFT_MENU_SEARCH_STATE = 'search';
export const LEFT_MENU_PEOPLE_STATE = 'people';
export const LEFT_MENU_GROUP_STATE = 'group';
export const LEFT_MENU_CREATE_STATE = 'create';
export const LEFT_MENU_TOGGLE_STATE = 'toggle';
